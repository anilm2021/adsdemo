/*
 * ER/Studio Data Architect SQL Code Generation
 * Project :      HR Database (logical complete).DM1
 *
 * Date Created : Wednesday, February 16, 2022 06:15:05
 * Target DBMS : Microsoft SQL Server 2017
 */

/* 
 * USER: IDERA_ADMIN 
 */

exec sp_grantdbaccess 'IDERA_ADMIN', 'IDERA_ADMIN'
go

/* 
 * USER: IDERA_MGR 
 */

exec sp_grantdbaccess 'IDERA_MGR', 'IDERA_MGR'
go

/* 
 * SEQUENCE: MYSEQUENCE 
 */



IF OBJECT_ID('MYSEQUENCE') IS NOT NULL
    PRINT '<<< CREATED SEQUENCE MYSEQUENCE >>>'
ELSE
    PRINT '<<< FAILED CREATING SEQUENCE MYSEQUENCE >>>'
go


/* 
 * TABLE: ADR 
 */

CREATE TABLE ADR(
    ADR_ID      char(10)    NOT NULL,
    ADR_LN_1    char(10)    NULL,
    ADR_LN_2    char(10)    NULL,
    CTY         char(10)    NULL,
    PST_CDE     char(10)    NULL,
    CTY_1       char(10)    NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (ADR_ID)
)

go


IF OBJECT_ID('ADR') IS NOT NULL
    PRINT '<<< CREATED TABLE ADR >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE ADR >>>'
go

/* 
 * TABLE: AGY 
 */

CREATE TABLE AGY(
    ID          int              IDENTITY(1,1),
    AGYNME      varchar(10)      NULL,
    MAINCCT     varchar(100)     NULL,
    CONTRACT    varchar(1000)    NULL,
    CONSTRAINT PK9 PRIMARY KEY NONCLUSTERED (ID)
)

go


IF OBJECT_ID('AGY') IS NOT NULL
    PRINT '<<< CREATED TABLE AGY >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE AGY >>>'
go

/* 
 * TABLE: CNR 
 */

CREATE TABLE CNR(
    EMPNMB      varchar(12)      NOT NULL,
    DAYRAT      numeric(7, 0)    NULL,
    AGENCYID    int              NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (EMPNMB)
)

go


IF OBJECT_ID('CNR') IS NOT NULL
    PRINT '<<< CREATED TABLE CNR >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE CNR >>>'
go

/* 
 * TABLE: DEP 
 */

CREATE TABLE DEP(
    ID     char(10)    NOT NULL,
    NME    char(10)    NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (ID)
)

go


IF OBJECT_ID('DEP') IS NOT NULL
    PRINT '<<< CREATED TABLE DEP >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DEP >>>'
go

/* 
 * TABLE: EMP 
 */

CREATE TABLE EMP(
    EMPNMB      varchar(12)       NOT NULL,
    EMPTYP      char(1)           NOT NULL,
    STTDTE      date              NULL,
    PHONENMB    numeric(15, 0)    NULL,
    FRSNME      varchar(200)      NOT NULL,
    SURNAME     varchar(200)      NOT NULL,
    MEMBER      char(10)          NOT NULL,
    MANAGER     char(10)          NULL,
    ADR_ID      char(10)          NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (EMPNMB)
)

go


IF OBJECT_ID('EMP') IS NOT NULL
    PRINT '<<< CREATED TABLE EMP >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE EMP >>>'
go

/* 
 * TABLE: EMPSKLMTX 
 */

CREATE TABLE EMPSKLMTX(
    ID        char(10)       NOT NULL,
    EMPNMB    varchar(12)    NOT NULL,
    LVL       char(10)       NULL,
    CONSTRAINT PK13 PRIMARY KEY NONCLUSTERED (ID, EMPNMB)
)

go


IF OBJECT_ID('EMPSKLMTX') IS NOT NULL
    PRINT '<<< CREATED TABLE EMPSKLMTX >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE EMPSKLMTX >>>'
go

/* 
 * TABLE: FULL_TIMEEMP 
 */

CREATE TABLE FULL_TIMEEMP(
    EMPNMB     varchar(12)    NOT NULL,
    SALARY     money          NULL,
    VCTDAYS    int            NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (EMPNMB)
)

go


IF OBJECT_ID('FULL_TIMEEMP') IS NOT NULL
    PRINT '<<< CREATED TABLE FULL_TIMEEMP >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE FULL_TIMEEMP >>>'
go

/* 
 * TABLE: SKL 
 */

CREATE TABLE SKL(
    ID     char(10)    NOT NULL,
    NME    char(10)    NOT NULL,
    DSP    char(10)    NULL,
    CTY    char(10)    NULL,
    CONSTRAINT PK5 PRIMARY KEY NONCLUSTERED (ID)
)

go


IF OBJECT_ID('SKL') IS NOT NULL
    PRINT '<<< CREATED TABLE SKL >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE SKL >>>'
go

/* 
 * TABLE: SKLTRGCRS 
 */

CREATE TABLE SKLTRGCRS(
    ID       char(10)    NOT NULL,
    CRSID    char(10)    NOT NULL,
    CONSTRAINT PK14 PRIMARY KEY NONCLUSTERED (ID, CRSID)
)

go


IF OBJECT_ID('SKLTRGCRS') IS NOT NULL
    PRINT '<<< CREATED TABLE SKLTRGCRS >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE SKLTRGCRS >>>'
go

/* 
 * TABLE: TRGCRS 
 */

CREATE TABLE TRGCRS(
    CRSID       char(10)    NOT NULL,
    TITLE       char(10)    NULL,
    SYNOPSIS    char(10)    NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (CRSID)
)

go


IF OBJECT_ID('TRGCRS') IS NOT NULL
    PRINT '<<< CREATED TABLE TRGCRS >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE TRGCRS >>>'
go

/* 
 * TABLE: TRGHST 
 */

CREATE TABLE TRGHST(
    EMPNMB           varchar(12)    NOT NULL,
    CRSID            char(10)       NOT NULL,
    COMPLETIONDTE    char(10)       NULL,
    RESULT           char(10)       NULL,
    CONSTRAINT PK12 PRIMARY KEY NONCLUSTERED (EMPNMB, CRSID)
)

go


IF OBJECT_ID('TRGHST') IS NOT NULL
    PRINT '<<< CREATED TABLE TRGHST >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE TRGHST >>>'
go

/* 
 * VIEW: VW_Employee 
 */

CREATE VIEW VW_Employee  AS
SELECT Em.EMPNMB, Em.EMPTYP, Em.STTDTE, Em.PHONENMB, Em.FRSNME, Em.SURNAME, Em.MANAGER, Fu.SALARY, Co.DAYRAT
FROM EMP Em, FULL_TIMEEMP Fu, CNR Co
WHERE Fu."Employee Number" = Em."Employee Number" AND Co."Employee Number" = Em."Employee Number"
go

IF OBJECT_ID('VW_Employee') IS NOT NULL
    PRINT '<<< CREATED VIEW VW_Employee >>>'
ELSE
    PRINT '<<< FAILED CREATING VIEW VW_Employee >>>'
go

/* 
 * TABLE: CNR 
 */

ALTER TABLE CNR ADD CONSTRAINT FK 
    FOREIGN KEY (EMPNMB)
    REFERENCES EMP(EMPNMB)
go

ALTER TABLE CNR ADD CONSTRAINT FK2 
    FOREIGN KEY (AGENCYID)
    REFERENCES AGY(ID)
go


/* 
 * TABLE: EMP 
 */

ALTER TABLE EMP ADD CONSTRAINT FK1 
    FOREIGN KEY (ADR_ID)
    REFERENCES ADR(ADR_ID)
go

ALTER TABLE EMP ADD CONSTRAINT FK3 
    FOREIGN KEY (MANAGER)
    REFERENCES DEP(ID)
go

ALTER TABLE EMP ADD CONSTRAINT FK4 
    FOREIGN KEY (MEMBER)
    REFERENCES DEP(ID)
go


/* 
 * TABLE: EMPSKLMTX 
 */

ALTER TABLE EMPSKLMTX ADD CONSTRAINT FK5 
    FOREIGN KEY (EMPNMB)
    REFERENCES EMP(EMPNMB)
go

ALTER TABLE EMPSKLMTX ADD CONSTRAINT FK7 
    FOREIGN KEY (ID)
    REFERENCES SKL(ID)
go


/* 
 * TABLE: FULL_TIMEEMP 
 */

ALTER TABLE FULL_TIMEEMP ADD CONSTRAINT FK 
    FOREIGN KEY (EMPNMB)
    REFERENCES EMP(EMPNMB)
go


/* 
 * TABLE: SKLTRGCRS 
 */

ALTER TABLE SKLTRGCRS ADD CONSTRAINT RefSKL26 
    FOREIGN KEY (ID)
    REFERENCES SKL(ID)
go

ALTER TABLE SKLTRGCRS ADD CONSTRAINT RefTRGCRS27 
    FOREIGN KEY (CRSID)
    REFERENCES TRGCRS(CRSID)
go


/* 
 * TABLE: TRGHST 
 */

ALTER TABLE TRGHST ADD CONSTRAINT FK6 
    FOREIGN KEY (EMPNMB)
    REFERENCES FULL_TIMEEMP(EMPNMB)
go

ALTER TABLE TRGHST ADD CONSTRAINT FK9 
    FOREIGN KEY (CRSID)
    REFERENCES TRGCRS(CRSID)
go


/* 
 * FUNCTION: GET_HIGHRATECONTRACTORS 
 */

CREATE FUNCTION "GET_HIGHRATECONTRACTORS"(@RATE int)
RETURNS NUMBER(38,0)

AS 
BEGIN select distinct EMPNMB
      from CNR
      where DAYRAT > @RATE

END;


go
IF OBJECT_ID('GET_HIGHRATECONTRACTORS') IS NOT NULL
    PRINT '<<< CREATED FUNCTION GET_HIGHRATECONTRACTORS >>>'
ELSE
    PRINT '<<< FAILED CREATING FUNCTION GET_HIGHRATECONTRACTORS >>>'
go


/* 
 * PROCEDURE: GetProductDesc 
 */

CREATE PROCEDURE GetProductDesc
AS
BEGIN
SET NOCOUNT ON
INSERT INTO CNR VALUES ('Jane Doe','Jon Smith')
 
END


go
IF OBJECT_ID('GetProductDesc') IS NOT NULL
    PRINT '<<< CREATED PROCEDURE GetProductDesc >>>'
ELSE
    PRINT '<<< FAILED CREATING PROCEDURE GetProductDesc >>>'
go


